import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

public class ListDemo{
	public static void main(String[] args){
		List<String> memberList = new ArrayList();
		memberList.add("Anli");
		memberList.add("Siling");
		System.out.println("-------------------------");	
		for(Iterator<String> iter = memberList.iterator(); iter.hasNext();){
 			System.out.println(iter.next());
        	}
		System.out.println("-------------------------");	
	}
}
